package com.yourosoft.protektoid.services;


import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.VpnService;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.PowerManager;
import android.os.Process;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;
import android.util.Log;

import com.yourosoft.protektoid.R;
import com.yourosoft.protektoid.models.AppHasProtektoidPermModel;
import com.yourosoft.protektoid.models.AppModel;
import com.yourosoft.protektoid.models.Constants;
import com.yourosoft.protektoid.firewall.FilteringConfiguration;
import com.yourosoft.protektoid.models.LogDbModel;
import com.yourosoft.protektoid.models.ProtektoidPermMapping;
import com.yourosoft.protektoid.models.SettingsModel;

import java.io.File;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.yourosoft.protektoid.firewall.Packet;

public class NetService extends VpnService  {

    static {
        System.loadLibrary("ProtektoidWall");
    }
    private static final String TAG = "Protektoid.NetService";

    private State state = State.none;
    private boolean user_foreground = true;
    private boolean last_connected = false;
    private boolean powersaving = false;

    private NetServiceBuilder last_builder = null;
    private ParcelFileDescriptor vpn = null;

    private Map<Integer, Integer> mapUidKnown = new HashMap<>();
    private volatile Looper commandLooper;
    private volatile Looper logLooper;
    private volatile CommandHandler commandHandler;
    private volatile LogHandler logHandler;

    private static final int NOTIFY_DISABLED = 3;
    private static final int NOTIFY_AUTOSTART = 4;
    private static final int NOTIFY_ERROR = 5;

    public static final String EXTRA_COMMAND = "Command";
    private static final String EXTRA_REASON = "Reason";
    public static final String EXTRA_NETWORK = "Network";
    public static final String EXTRA_UID = "UID";
    public static final String EXTRA_PACKAGE = "Package";
    public static final String EXTRA_BLOCKED = "Blocked";

    private static final int MSG_SERVICE_INTENT = 0;
    private static final int MSG_PACKET = 4;
    private static final int MSG_USAGE = 5;

    private enum State {none, waiting, enforcing, stats}

    public enum Command {run, start, reload, stop, stats, set, householding, watchdog}

    private static volatile PowerManager.WakeLock wlInstance = null;

    private static final String ACTION_HOUSE_HOLDING = "eu.faircode.netguard.HOUSE_HOLDING";
    private static final String ACTION_WATCHDOG = "eu.faircode.netguard.WATCHDOG";

    private native void jni_init();

    private native void jni_start(int tun, boolean fwd53, int loglevel);

    private native void jni_stop(int tun, boolean clr);

    private native int jni_get_mtu();

    private static native void jni_pcap(String name, int record_size, int file_size);

    private native void jni_socks5(String addr, int port, String username, String password);

    private native void jni_done();
    public static void setPcap(boolean enabled, Context context) {
        File pcap = (enabled ? new File(context.getCacheDir(), "protektoid.pcap") : null);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String r = prefs.getString("pcap_record_size", null);
        if (TextUtils.isEmpty(r))
            r = "64";
        String f = prefs.getString("pcap_file_size", null);
        if (TextUtils.isEmpty(f))
            f = "2";
        int record_size = Integer.parseInt(r);
        int file_size = Integer.parseInt(f) * 1024 * 1024;
        jni_pcap(pcap == null ? null : pcap.getAbsolutePath(), record_size, file_size);
    }

    synchronized private static PowerManager.WakeLock getLock(Context context) {
        if (wlInstance == null) {
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            wlInstance = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, context.getString(R.string.app_name) + " wakelock");
            wlInstance.setReferenceCounted(true);
        }
        return wlInstance;
    }

    private final class CommandHandler extends Handler {
        public int queue = 0;

        public CommandHandler(Looper looper) {
            super(looper);
        }


        public void queue(Intent intent) {
            synchronized (this) {
                queue++;
            }
            Message msg = commandHandler.obtainMessage();
            msg.obj = intent;
            msg.what = MSG_SERVICE_INTENT;
            commandHandler.sendMessage(msg);
        }

        @Override
        public void handleMessage(Message msg) {
            try {
                switch (msg.what) {
                    case MSG_SERVICE_INTENT:
                        handleIntent((Intent) msg.obj);
                        break;
                    default:
                        Log.e(TAG, "Unknown command message=" + msg.what);
                }
            } catch (Throwable ex) {
                Log.e(TAG, ex.toString() + "\n" + Log.getStackTraceString(ex));
            } finally {
                synchronized (this) {
                    queue--;
                }
                try {
                    PowerManager.WakeLock wl = getLock(NetService.this);
                    if (wl.isHeld())
                        wl.release();
                    else
                        Log.w(TAG, "Wakelock under-locked");
                    Log.i(TAG, "Messages=" + hasMessages(0) + " wakelock=" + wlInstance.isHeld());
                } catch (Throwable ex) {
                    Log.e(TAG, ex.toString() + "\n" + Log.getStackTraceString(ex));
                }
            }
        }

        private void handleIntent(Intent intent) {
            final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NetService.this);

            Command cmd = (Command) intent.getSerializableExtra(EXTRA_COMMAND);
            String reason = intent.getStringExtra(EXTRA_REASON);
            Log.i(TAG, "Executing intent=" + intent + " command=" + cmd + " reason=" + reason +
                    " vpn=" + (vpn != null) + " user=" + (Process.myUid() / 100000));

            // Check if foreground
            if (cmd != Command.stop)
                if (!user_foreground) {
                    Log.i(TAG, "Command " + cmd + "ignored for background user");
                    return;
                }



            // Watchdog
            if (cmd == Command.start || cmd == Command.reload) {
                Intent watchdogIntent = new Intent(NetService.this, NetService.class);
                watchdogIntent.setAction(ACTION_WATCHDOG);
                PendingIntent pi = PendingIntent.getService(NetService.this, 1, watchdogIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                am.cancel(pi);

                int watchdog = Integer.parseInt(prefs.getString("watchdog", "0"));
                if (watchdog > 0) {
                    Log.i(TAG, "Watchdog " + watchdog + " minutes");
                    am.setInexactRepeating(AlarmManager.RTC, SystemClock.elapsedRealtime() + watchdog * 60 * 1000, watchdog * 60 * 1000, pi);
                }
            }

            try {
                switch (cmd) {
                    case run:
                        run();
                        break;

                    case start:
                        start();
                        break;

                    case reload:
                        reload();
                        break;

                    case stop:
                        stop();
                        break;

                    case householding:
                        householding(intent);
                        break;



                    default:
                        Log.e(TAG, "Unknown command=" + cmd);
                }

            } catch (Throwable ex) {
                Log.e(TAG, ex.toString() + "\n" + Log.getStackTraceString(ex));
                if (cmd == Command.start || cmd == Command.reload) {
                    if (VpnService.prepare(NetService.this) == null) {
                        Log.w(TAG, "VPN not prepared connected=" + last_connected);
                    }
                }

            }
        }

        private void run() {
            if (state == State.none) {
                state = State.waiting;
                Log.d(TAG, "Start foreground state=" + state.toString());
            }
        }

        private void start() {
            if (vpn == null) {
                if (state != State.none) {
                    Log.d(TAG, "Stop foreground state=" + state.toString());
                    stopForeground(true);
                }
                state = State.enforcing;
                Log.d(TAG, "Start foreground state=" + state.toString());


                last_builder = getBuilder( );
                vpn = startVPN(last_builder);
                if (vpn == null)
                    throw new IllegalStateException("start failed");

                startNative(vpn );

                removeWarningNotifications();
            }else{
                reload( );

            }
        }

        private void reload() {

            if (vpn == null) {
                start(); return;
            }
            if (state != State.enforcing) {
                if (state != State.none) {
                    Log.d(TAG, "Stop foreground state=" + state.toString());
                    stopForeground(true);
                }
                state = State.enforcing;
                Log.d(TAG, "Start foreground state=" + state.toString());
            }

            NetServiceBuilder builder = getBuilder( );

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                last_builder = builder;
                Log.i(TAG, "Legacy restart");

                if (vpn != null) {
                    stopNative(vpn, false);
                    stopVPN(vpn);
                    vpn = null;
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ignored) {
                    }
                }
                vpn = startVPN(last_builder);

            } else {
                if (vpn != null  && builder.equals(last_builder)) {
                    Log.i(TAG, "Native restart");
                    stopNative(vpn, false);

                } else {
                    last_builder = builder;
                    Log.i(TAG, "VPN restart");

                    // Attempt seamless handover
                    ParcelFileDescriptor prev = vpn;
                    vpn = startVPN(builder);

                    if (prev != null && vpn == null) {
                        Log.w(TAG, "Handover failed");
                        stopNative(prev, false);
                        stopVPN(prev);
                        prev = null;
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException ignored) {
                        }
                        vpn = startVPN(last_builder);
                        if (vpn == null)
                            throw new IllegalStateException("Handover failed");
                    }

                    if (prev != null) {
                        stopNative(prev, false);
                        stopVPN(prev);
                    }
                }
            }

            if (vpn == null)
                throw new IllegalStateException("start failed");

            startNative(vpn);

            removeWarningNotifications();
        }

        private void householding(Intent intent) {
                // Keep log records for three days
            LogDbModel.getInstance(NetService.this).cleanupLog(new Date().getTime() - 3 * 24 * 3600 * 1000L);
        }


        private void stop() {
            if (vpn != null) {
                stopNative(vpn, true);
                stopVPN(vpn);
                vpn = null;
                unprepare();
            }
            if (state == State.enforcing) {
                Log.d(TAG, "Stop foreground state=" + state.toString());
                stopForeground(true);
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NetService.this);
                if (prefs.getBoolean("show_stats", false)) {
                    state = State.waiting;
                    Log.d(TAG, "Start foreground state=" + state.toString());
                } else
                    state = State.none;
            }
        }

    }
    public static List<InetAddress> getDns(Context context) {
        List<InetAddress> listDns = new ArrayList<>();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String vpnDns1 = prefs.getString("dns", null);
        String vpnDns2 = prefs.getString("dns2", null);
        Log.i(TAG, " VPN1=" + vpnDns1 + " VPN2=" + vpnDns2);

        if (vpnDns1 != null)
            try {
                InetAddress dns = InetAddress.getByName(vpnDns1);
                if (!(dns.isLoopbackAddress() || dns.isAnyLocalAddress()))
                    listDns.add(dns);
            } catch (Throwable ignored) {
            }

        if (vpnDns2 != null)
            try {
                InetAddress dns = InetAddress.getByName(vpnDns2);
                if (!(dns.isLoopbackAddress() || dns.isAnyLocalAddress()))
                    listDns.add(dns);
            } catch (Throwable ignored) {
            }

        return listDns;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private ParcelFileDescriptor startVPN(NetServiceBuilder builder) throws SecurityException {
        Log.w(TAG, "startVPN");
        try {
            return builder.establish();
        } catch (SecurityException ex) {
            throw ex;
        } catch (Throwable ex) {
            Log.e(TAG, ex.toString() + "\n" + Log.getStackTraceString(ex));
            return null;
        }
    }

    private NetServiceBuilder getBuilder() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean ip6 = prefs.getBoolean("ip6", true);
        // Build VPN service
        NetServiceBuilder builder = new NetServiceBuilder();
        builder.setSession("Protektoid");

        // VPN address
        String vpn4 = prefs.getString("vpn4", "10.1.10.1");
        Log.i(TAG, "vpn4=" + vpn4);
        builder.addAddress(vpn4, 32);
        if (ip6) {
            String vpn6 = prefs.getString("vpn6", "fd00:1:fd00:1:fd00:1:fd00:1");
            Log.i(TAG, "vpn6=" + vpn6);
            builder.addAddress(vpn6, 128);
        }

        builder.addRoute("0.0.0.0", 0);

        Log.i(TAG, "IPv6=" + ip6);
        if (ip6)
            builder.addRoute("0:0:0:0:0:0:0:0", 0);

        // MTU
        int mtu = jni_get_mtu();
        Log.i(TAG, "MTU=" + mtu);
        builder.setMtu(mtu);


        return builder;
    }

    private void startNative(ParcelFileDescriptor vpn) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NetService.this);
        boolean log = prefs.getBoolean("log", false);
        boolean log_app = prefs.getBoolean("log_app", false);

        Log.i(TAG, "Start native log=" + log + "/" + log_app  );

        int prio = Integer.parseInt(prefs.getString("loglevel", Integer.toString(Log.WARN)));
        if (prefs.getBoolean("socks5_enabled", false))
            jni_socks5(
                    prefs.getString("socks5_addr", ""),
                    Integer.parseInt(prefs.getString("socks5_port", "0")),
                    prefs.getString("socks5_username", ""),
                    prefs.getString("socks5_password", ""));
        else
            jni_socks5("", 0, "", "");
        jni_start(vpn.getFd(), false, prio);
    }

    private void stopNative(ParcelFileDescriptor vpn, boolean clear) {
        Log.i(TAG, "Stop native clear=" + clear);
        try {
            jni_stop(vpn.getFd(), clear);
        } catch (Throwable ex) {
            // File descriptor might be closed
            Log.e(TAG, ex.toString() + "\n" + Log.getStackTraceString(ex));
            jni_stop(-1, clear);
        }
    }

    private void unprepare() {
        FilteringConfiguration.get_instance().clear();
    }


    public static void resetPackage(Context context, String package_name) {
        PackageManager pm = context.getPackageManager();
        try {
            ApplicationInfo info = pm.getApplicationInfo(package_name, 0);
            FilteringConfiguration.get_instance().remove(info.uid);
        }catch (Exception e) {}

    }

    private void stopVPN(ParcelFileDescriptor pfd) {
        Log.i(TAG, "Stopping");
        try {
            pfd.close();
        } catch (IOException ex) {
            Log.e(TAG, ex.toString() + "\n" + Log.getStackTraceString(ex));
        }
    }

    // Called from native code
    private void nativeExit(String reason) {
        Log.w(TAG, "Native exit reason=" + reason);
    }

    // Called from native code
    private void nativeError(int error, String message) {
        Log.w(TAG, "Native error " + error + ": " + message);
    }

    // Called from native code
    private void logPacket(Packet packet) {
        Message msg = logHandler.obtainMessage();
        msg.obj = packet;
        msg.what = MSG_PACKET;
        msg.arg1 = 0 ; //TODO : remove?
        msg.arg2 = 0 ; //TODO : remove?
        logHandler.sendMessage(msg);
    }

    private boolean isSupported(int protocol) {
        return (protocol == 1 /* ICMPv4 */ ||
                protocol == 59 /* ICMPv6 */ ||
                protocol == 6 /* TCP */ ||
                protocol == 17 /* UDP */);
    }

    // Called from native code
    private boolean isAddressAllowed(Packet packet) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String mAppPackage = null;
        String mAppPackageTmp = null;
        String mAppLabel = null;
        ArrayList<String> app = null;
        List<ArrayList<String>> apps = null;
        try {
            apps = AppModel.getApplicationNames(packet.uid, NetService.this);
            app = apps.get(0);
            if(apps.size() <= 1) apps=null;
            mAppPackage = app.get(0);
            mAppLabel = app.get(1);
        }catch(Exception e){
            e.printStackTrace();
        }

        boolean cached=false;
        Iterator<ArrayList<String>> iter;
        AppHasProtektoidPermModel mAppHasProtektoidModel;
        AppHasProtektoidPermModel mAppHasProtektoidModelTmp;
        boolean trusted;
        boolean disabled;
        boolean nLiked;

        if (! SettingsModel.isNetworkFilteringEnforced(this)) {
            Log.w(TAG, "Allowing by default " + packet);
            packet.allowed = true;
        }else{
            FilteringConfiguration fc = FilteringConfiguration.get_instance();
            packet.allowed = false;
            if(mAppPackage != null) {
                if(fc.is_allowed(packet.uid) ) {
                    packet.allowed = true;
                    cached = true;
                }else{
                    if(fc.is_denied(packet.uid) ) {
                        packet.allowed = false;
                        cached = true;
                    }else{
                        trusted=true;
                        nLiked=false;
                        disabled=false;
                        mAppHasProtektoidModel = null;
                        if(apps != null) {
                            iter = apps.iterator();
                            while(iter.hasNext()) {
                                app =iter.next();
                                mAppPackageTmp = app.get(0);
                                mAppHasProtektoidModelTmp = new AppHasProtektoidPermModel(this).getRow(mAppPackageTmp, ProtektoidPermMapping.NETWORK_PERM);
                                if( mAppHasProtektoidModelTmp != null) {
                                    mAppHasProtektoidModel = mAppHasProtektoidModelTmp;
                                    trusted &= mAppHasProtektoidModel.isTrusted();
                                    disabled |= mAppHasProtektoidModel.isDisabled();
                                    nLiked |= mAppHasProtektoidModel.notLiked();
                                    if(mAppHasProtektoidModel.isDisabled()  | mAppHasProtektoidModel.notLiked() ){
                                        mAppPackage = mAppPackageTmp;
                                        mAppLabel = app.get(1);
                                    }else {
                                        if(trusted){
                                            mAppPackage = mAppPackageTmp;
                                            mAppLabel = app.get(1);
                                        }
                                    }
                                }
                            }

                        }else {
                            mAppHasProtektoidModel = new AppHasProtektoidPermModel(this).getRow(mAppPackage, ProtektoidPermMapping.NETWORK_PERM);

                            trusted=mAppHasProtektoidModel.isTrusted();
                            disabled=mAppHasProtektoidModel.isDisabled();
                            nLiked = mAppHasProtektoidModel.notLiked();
                        }


                        if( mAppHasProtektoidModel == null) {
                            //shall not happens as it means either app is not indexed or its NET usage is not discovered
                            packet.allowed = SettingsModel.isNetworkUnknownAppAllowed(this);
                            cached = true; //to avoid other notifications
                            Log.e(TAG, "APP "+mAppPackage+" is not discovered for net usage. allowed="+packet.allowed);

                                Intent notificationIntent = new Intent(this.getApplicationContext(), NotificationService.class);
                                notificationIntent.putExtra(NotificationService.ARG_ACTION, NotificationService.ACTION_NOTIFY_APP_NETWORK_UNKNOWN);
                                notificationIntent.putExtra(NotificationService.ARG_PACKAGE, mAppPackage);
                                notificationIntent.putExtra(NotificationService.ARG_APP_LABEL, mAppLabel);
                                notificationIntent.putExtra(NotificationService.ARG_APP_UID, ""+packet.uid);
                            notificationIntent.putExtra(NotificationService.ARG_SHARED_UID, apps !=null);
                                startService(notificationIntent);
                        }else{

                            if( trusted) {
                                packet.allowed = true;
                            }else {
                                if (!nLiked && !disabled) {

                                    boolean isSysApp = AppModel.isSystemApp(this,mAppPackage);
                                    packet.allowed = true;
                                    if( (isSysApp && SettingsModel.isNetworkDefaultSystemKnownAppBlocking(this)) ||
                                            (!isSysApp && SettingsModel.isNetworkDefaultUserKnownAppBlocking(this))) {
                                        packet.allowed = false;
                                    }
                                    Log.i(TAG, "Using default policy: allowed="+  packet.allowed );
                                }
                            }
                        }
                        if( packet.allowed == true) {
                            fc.allow(packet.uid,System.currentTimeMillis() +Constants.FIREWALL_VALIDITY);
                        }else {
                            fc.deny(packet.uid,System.currentTimeMillis() +Constants.FIREWALL_VALIDITY);

                        }
                  }

                }
            }
        }

        Log.i(TAG, "status for " + mAppPackage+"/"+mAppLabel+" (uid:"+packet.uid+"): "+packet.allowed+ ", cached: "+cached);


        if (packet.uid < 2000 &&
                !last_connected && isSupported(packet.protocol)) {
            // Allow system applications in disconnected state
            packet.allowed = true;
            Log.w(TAG, "Allowing disconnected system " + packet);

        } else if (packet.uid < 2000 &&
                !mapUidKnown.containsKey(packet.uid) && isSupported(packet.protocol)) {
            // Allow unknown system traffic
            packet.allowed = true;
            Log.w(TAG, "Allowing unknown system " + packet);

        } 
        if (!packet.allowed) { 

            if(!cached) {
                Intent notificationIntent = new Intent(this.getApplicationContext(), NotificationService.class);
                notificationIntent.putExtra(NotificationService.ARG_ACTION, NotificationService.ACTION_NOTIFY_APP_NETWORK_BLOCKED);
                notificationIntent.putExtra(NotificationService.ARG_PACKAGE, mAppPackage);
                notificationIntent.putExtra(NotificationService.ARG_APP_LABEL, mAppLabel);
                notificationIntent.putExtra(NotificationService.ARG_APP_UID, ""+packet.uid);
                notificationIntent.putExtra(NotificationService.ARG_SHARED_UID, apps !=null);
                startService(notificationIntent);
            }
        }

        logPacket(packet);

        return packet.allowed;
    }



    @Override
    public void onCreate() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        // Native init
        jni_init();
        boolean pcap = prefs.getBoolean("pcap", false);
        setPcap(pcap, this);
        super.onCreate();

        HandlerThread commandThread = new HandlerThread(getString(R.string.app_name) + " command");
        HandlerThread logThread = new HandlerThread(getString(R.string.app_name) + " log");
        commandThread.start();
        logThread.start();

        commandLooper = commandThread.getLooper();
        logLooper = logThread.getLooper();
        commandHandler = new CommandHandler(commandLooper);
        logHandler = new LogHandler(logLooper);

        // Setup house holding
        Intent alarmIntent = new Intent(this, NetService.class);
        alarmIntent.setAction(ACTION_HOUSE_HOLDING);
        PendingIntent pi = PendingIntent.getService(this, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        am.setInexactRepeating(AlarmManager.RTC, SystemClock.elapsedRealtime() + 60 * 1000, AlarmManager.INTERVAL_HALF_DAY, pi);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Received " + intent);
        NetService.logExtras(intent);

        // Check for set command
        if (intent != null && intent.hasExtra(EXTRA_COMMAND) &&
                intent.getSerializableExtra(EXTRA_COMMAND) == Command.set) {
            set(intent);
            return START_STICKY;
        }

        // Keep awake
        getLock(this).acquire();

        // Get state

        boolean enabled = SettingsModel.isNetworkFilteringEnable(this);

        // Handle service restart
        if (intent == null) {
            Log.i(TAG, "Restart");

            // Recreate intent
            intent = new Intent(this, NetService.class);
            intent.putExtra(EXTRA_COMMAND, enabled ? Command.start : Command.stop);
        }

        if (ACTION_HOUSE_HOLDING.equals(intent.getAction()))
            intent.putExtra(EXTRA_COMMAND, Command.householding);
        if (ACTION_WATCHDOG.equals(intent.getAction()))
            intent.putExtra(EXTRA_COMMAND, Command.watchdog);

        Command cmd = (Command) intent.getSerializableExtra(EXTRA_COMMAND);
        if (cmd == null)
            intent.putExtra(EXTRA_COMMAND, enabled ? Command.start : Command.stop);
        String reason = intent.getStringExtra(EXTRA_REASON);
        Log.i(TAG, "Start intent=" + intent + " command=" + cmd + " reason=" + reason +
                " vpn=" + (vpn != null) + " user=" + (Process.myUid() / 100000));

        commandHandler.queue(intent);
        return START_STICKY;
    }

    private void set(Intent intent) {
        // Get arguments
        int uid = intent.getIntExtra(EXTRA_UID, 0);
        String network = intent.getStringExtra(EXTRA_NETWORK);
        String pkg = intent.getStringExtra(EXTRA_PACKAGE);
        boolean blocked = intent.getBooleanExtra(EXTRA_BLOCKED, false);
        Log.i(TAG, "Set " + pkg + " " + network + "=" + blocked);

        // Get defaults
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(NetService.this);
        boolean default_wifi = settings.getBoolean("whitelist_wifi", true);
        boolean default_other = settings.getBoolean("whitelist_other", true);

        // Update setting
        SharedPreferences prefs = getSharedPreferences(network, Context.MODE_PRIVATE);
        if (blocked == ("wifi".equals(network) ? default_wifi : default_other))
            prefs.edit().remove(pkg).apply();
        else
            prefs.edit().putBoolean(pkg, blocked).apply();

        // Apply rules
        NetService.reload("notification", NetService.this);

    }

    @Override
    public void onRevoke() {
        Log.i(TAG, "Revoke");

        // Disable firewall (will result in stop command)
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.edit().putBoolean("enabled", false).apply();


        super.onRevoke();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "Destroy");

        commandLooper.quit();



        try {
            if (vpn != null) {
                stopNative(vpn, true);
                stopVPN(vpn);
                vpn = null;
                unprepare();
            }
        } catch (Throwable ex) {
            Log.e(TAG, ex.toString() + "\n" + Log.getStackTraceString(ex));
        }

        jni_done();

        super.onDestroy();
    }


    private void removeWarningNotifications() {
        NotificationManagerCompat.from(this).cancel(NOTIFY_DISABLED);
        NotificationManagerCompat.from(this).cancel(NOTIFY_AUTOSTART);
        NotificationManagerCompat.from(this).cancel(NOTIFY_ERROR);
    }

    public static void run(String reason, Context context) {
        Intent intent = new Intent(context, NetService.class);
        intent.putExtra(EXTRA_COMMAND, Command.run);
        intent.putExtra(EXTRA_REASON, reason);
        context.startService(intent);
    }

    public static void start(String reason, Context context) {
        Intent intent = new Intent(context, NetService.class);
        intent.putExtra(EXTRA_COMMAND, Command.start);
        intent.putExtra(EXTRA_REASON, reason);
        context.startService(intent);
    }

    public static void reload(String reason, Context context) {
        if(SettingsModel.isNetworkFilteringEnable(context)) {
            Intent intent = new Intent(context, NetService.class);
            intent.putExtra(EXTRA_COMMAND, Command.reload);
            intent.putExtra(EXTRA_REASON, reason);
            context.startService(intent);
        }
    }

    public static void stop(String reason, Context context) {
        Intent intent = new Intent(context, NetService.class);
        intent.putExtra(EXTRA_COMMAND, Command.stop);
        intent.putExtra(EXTRA_REASON, reason);
        context.startService(intent);
    }

    /* ------------------------------------- */
    /*          Private Class                */
    /* ------------------------------------- */

    private final class LogHandler extends Handler {
        public LogHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            try {
                if (powersaving && (msg.what == MSG_PACKET || msg.what == MSG_USAGE))
                    return;

                switch (msg.what) {
                    case MSG_PACKET:
                        log((Packet) msg.obj);
                        break;


                    default:
                        Log.e(TAG, "Unknown log message=" + msg.what);
                }
            } catch (Throwable ex) {
                Log.e(TAG, ex.toString() + "\n" + Log.getStackTraceString(ex));
            }
        }

        private void log(Packet packet ) {
            // Get settings
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(NetService.this);
            LogDbModel dh = LogDbModel.getInstance(NetService.this);


            dh.insertLog(packet);

        }

    }


    /* ------------------------------------- */
    /*          Private Class                */
    /* ------------------------------------- */


    private class NetServiceBuilder extends VpnService.Builder {
        private NetworkInfo networkInfo;
        private List<String> listAddress = new ArrayList<>();
        private List<String> listRoute = new ArrayList<>();

        private NetServiceBuilder() {
            super();
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            networkInfo = cm.getActiveNetworkInfo();
        }


        @Override
        public boolean equals(Object obj) {
            NetServiceBuilder other = (NetServiceBuilder) obj;

            if (other == null)
                return false;

            if (this.networkInfo == null || other.networkInfo == null ||
                    this.networkInfo.getType() != other.networkInfo.getType())
                return false;


            if (this.listAddress.size() != other.listAddress.size())
                return false;

            if (this.listRoute.size() != other.listRoute.size())
                return false;


            for (String address : this.listAddress)
                if (!other.listAddress.contains(address))
                    return false;

            for (String route : this.listRoute)
                if (!other.listRoute.contains(route))
                    return false;


            return true;
        }
    }

    //UTILS

    public static void logExtras(Intent intent) {
        if (intent != null)
            logBundle(intent.getExtras());
    }

    public static void logBundle(Bundle data) {
        if (data != null) {
            Set<String> keys = data.keySet();
            StringBuilder stringBuilder = new StringBuilder();
            for (String key : keys) {
                Object value = data.get(key);
                stringBuilder.append(key)
                        .append("=")
                        .append(value)
                        .append(value == null ? "" : " (" + value.getClass().getSimpleName() + ")")
                        .append("\r\n");
            }
            Log.d(TAG, stringBuilder.toString());
        }
    }


}
