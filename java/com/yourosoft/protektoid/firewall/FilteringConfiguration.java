package com.yourosoft.protektoid.firewall;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by julien on 2016.11.24..
 */

public class FilteringConfiguration {
    private Map<Integer, Long> mapUidAllowed = new HashMap<>();
    private Map<Integer, Long> mapUidDenied = new HashMap<>();

    private static FilteringConfiguration instance;
    public static FilteringConfiguration get_instance() {
        if(instance == null) instance = new FilteringConfiguration();
        return instance;
    }

    public void clear() {

        mapUidAllowed.clear();
        mapUidDenied.clear();
    }

    public void remove(int uid) {

        mapUidDenied.remove(uid);
        mapUidAllowed.remove(uid);

    }

    public void allow(int uid, Long valid) {

        mapUidAllowed.put(uid, valid);

    }
    public void deny(int uid, Long valid) {

        mapUidDenied.put(uid, valid);

    }

    public boolean is_allowed(int uid) {
        return is_allowed(uid,true);
    }

    public boolean is_allowed(int uid, boolean onlyValid) {
        if(mapUidAllowed.containsKey(uid) ) {
            if(!onlyValid) return true;
            return mapUidAllowed.get(uid) > System.currentTimeMillis();
        }
        return false;
    }

    public boolean is_denied(int uid) {
        return is_denied(uid,true);
    }
    public boolean is_denied(int uid, boolean onlyValid) {
        if(mapUidDenied.containsKey(uid) ) {
            if(!onlyValid) return true;
            return mapUidDenied.get(uid) > System.currentTimeMillis();
        }
        return false;
    }



}
