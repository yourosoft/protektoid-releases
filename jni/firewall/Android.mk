APP_CFLAGS += -Wno-error=format-security
LOCAL_PATH := $(call my-dir)
APP_CFLAGS += -Wno-error=format-security

include $(CLEAR_VARS)
LOCAL_DISABLE_FORMAT_STRING_CHECKS := true
LOCAL_EXPORT_LDLIBS := -llog
LOCAL_LDLIBS := -llog

# Here we give our module name and source file(s)
LOCAL_MODULE    := ProtektoidWall
LOCAL_SRC_FILES := firewall.c icmp.c ip.c pcap.c tcp.c session.c udp.c util.c

include $(BUILD_SHARED_LIBRARY)